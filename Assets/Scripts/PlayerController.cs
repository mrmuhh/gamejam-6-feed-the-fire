﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private static readonly float SIZE_CHANGE_SPEED = 5f;
    private static readonly float PS_MAX_RATE = 350f;
    private static readonly float PS_MAX_SIZE = 0.5f;
    private static readonly float PS_MIN_SIZE = 0.1f;
    private static readonly float PS_MAX_RADIUS = 0.5f;
    private static readonly float PS_MIN_RADIUS = 0.2f;
    private static readonly float PLAYER_MAX_SIZE = 10f;
    private Rigidbody2D playerBody;
    private CircleCollider2D col;
    private float targetSize;
    public ParticleSystem ParticleSystem;
    public float scoreValue;

    private void Start()
    {
        playerBody = GetComponent<Rigidbody2D>();
        col = GetComponent<CircleCollider2D>();
        targetSize = gameObject.transform.localScale.x;
    }

    private void Update()
    {
        if (IsGameOver())
        {
            SceneManager.LoadScene("GameOverMenu");
        }
    }


    private bool IsGameOver()
    {
        return IsPlayerBelowViewport() || targetSize <= 0f;
    }

    private bool IsPlayerBelowViewport()
    {
        return Camera.main.WorldToViewportPoint(
            new Vector3(
                transform.position.x,
                transform.position.y + col.radius,
                transform.position.z)).y < 0f;

    }

    void FixedUpdate()
    {
        float size = Mathf.MoveTowards(gameObject.transform.localScale.x, targetSize, Time.deltaTime * SIZE_CHANGE_SPEED);
        size = Mathf.Min(size, PLAYER_MAX_SIZE);
        gameObject.transform.localScale = new Vector3(size, size, size);

        float sizeFactor = Mathf.InverseLerp(0.5f, PLAYER_MAX_SIZE, size);

        ParticleSystem.EmissionModule emission = ParticleSystem.emission;
        emission.rateOverTime = PS_MAX_RATE * sizeFactor;
        ParticleSystem.MainModule main = ParticleSystem.main;
        main.startSize = Mathf.Max(PS_MAX_SIZE * sizeFactor, PS_MIN_SIZE);

        ParticleSystem.ShapeModule shape = ParticleSystem.shape;
        shape.radius = Mathf.Max(PS_MAX_RADIUS * sizeFactor, PS_MIN_RADIUS);
    }

    public void ChangePlayerSize(float size)
    {
        targetSize += size;
    }

    public Vector3 GetPlayerPosition()
    {
        return gameObject.transform.position; 
    }
}