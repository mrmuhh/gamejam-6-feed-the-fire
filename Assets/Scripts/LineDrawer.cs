﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer : MonoBehaviour
{
    public GameObject linePrefab;
    private GameObject currentLine;
    private List<Vector2> pointerPos;

    private void Start()
    {
        pointerPos = new List<Vector2>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (currentLine == null)
            {
                currentLine = CreateNewLine();
            }
        }
        if (Input.GetMouseButton(0))
        {
            UpdateEndpoint(currentLine);
        }else
        {
            if (currentLine != null)
            {
                currentLine = null;
            }
        }
    }

    private GameObject CreateNewLine()
    {

        GameObject line = Instantiate(linePrefab, Vector3.zero, Quaternion.identity);
        LineRenderer lineRenderer = line.GetComponent<LineRenderer>();
        pointerPos.Clear();
        pointerPos.Add(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        pointerPos.Add(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        lineRenderer.SetPosition(0, pointerPos[0]);
        lineRenderer.SetPosition(1, pointerPos[1]);
        line.GetComponent<EdgeCollider2D>().points = pointerPos.ToArray();
        return line;
    }

    private void UpdateEndpoint(GameObject line)
    {
        if (line.gameObject != null) 
        { 
            LineRenderer lineRenderer = line.GetComponent<LineRenderer>();
            EdgeCollider2D collider = line.GetComponent<EdgeCollider2D>();
            pointerPos[1] = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            lineRenderer.SetPosition(1, pointerPos[1]);
            collider.points = pointerPos.ToArray();

            LineType lineType = ((pointerPos[0].x - pointerPos[1].x) > 0) ? LineType.ICE : LineType.FIRE;
            line.GetComponent<LineController>().SetLineType(lineType);
        }
    }

}
