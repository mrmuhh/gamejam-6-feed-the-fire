﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamBoundary : MonoBehaviour
{
    private float offset = 0f;

    void Awake()
    {

        PolygonCollider2D collider = GetComponent<PolygonCollider2D>();
        collider.points = new Vector2[]
        {
            Camera.main.ViewportToWorldPoint(new Vector3(0f, 0f, 0)),
            Camera.main.ViewportToWorldPoint(new Vector3(1f, 0f, 0)),
            Camera.main.ViewportToWorldPoint(new Vector3(1f, 1000f, 0)),
            Camera.main.ViewportToWorldPoint(new Vector3(0f, 1000f, 0))
        };

    }

    void Update()
    {
        Vector3 camCenter = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
        offset = camCenter.y;
        transform.position = new Vector3(transform.position.x, offset, transform.position.z);
    }
}