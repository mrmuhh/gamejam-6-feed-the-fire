﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public GameObject Player;
    public Font font;
    public static readonly string KEY_SCORE = "score";
    private static readonly int SCORE_MULTIPLIER = 100;
    private int score;
    private PlayerController playerController;
    private GUIStyle scoreStyle;
    private Rect scoreRect;

    void Start()
    {
        score = 0;
        playerController = Player.GetComponent<PlayerController>();

        int w = Screen.width;
        int h = Screen.height;
        scoreRect = new Rect(10f, 10f, w, h * 2 / 100);

        scoreStyle = new GUIStyle
        {
            alignment = TextAnchor.UpperLeft,
            fontSize = h * 2 / 100,
            font = font
        };
        scoreStyle.normal.textColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    }

    void FixedUpdate()
    {
        float playerHeight = playerController.GetPlayerPosition().y;
        if (playerHeight > score)
        {
            score = Mathf.RoundToInt(playerHeight);
            PlayerPrefs.SetInt(KEY_SCORE, score * SCORE_MULTIPLIER);
        }
    }

    void OnGUI()
    {
        GUI.Label(scoreRect, "" + score * SCORE_MULTIPLIER, scoreStyle);
    }
}
