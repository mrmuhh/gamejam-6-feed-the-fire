﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBoundary : MonoBehaviour
{
    public List<Vector2> newVerticies = new List<Vector2>();
    public EdgeCollider2D leftCollider;
    public EdgeCollider2D rightCollider;

    // Use this for initialization
    void Awake()
    {
        rightCollider.points = new Vector2[]
        {
            Camera.main.ViewportToWorldPoint(new Vector3(1f, 0f, 0)),
            Camera.main.ViewportToWorldPoint(new Vector3(1f, 1000f, 0))
        };

        leftCollider.points = new Vector2[]
        {
            Camera.main.ViewportToWorldPoint(new Vector3(0f, 0f, 0)),
            Camera.main.ViewportToWorldPoint(new Vector3(0f, 1000f, 0))
        };
    }
}
