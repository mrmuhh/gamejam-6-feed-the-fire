﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LineType { FIRE, ICE};

public class LineController : MonoBehaviour
{
    private static float SIZE_CHANGE = 0.5f;

    public Material fireMaterial;
    public Material iceMaterial;
    private LineType lineType;
    private LineRenderer lineRenderer;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineType = LineType.FIRE;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        col.collider.GetComponent<PlayerController>().ChangePlayerSize(GetSizeChange());
        if (gameObject != null)
        {
            Destroy(gameObject);
        }

    }

    public void SetLineType(LineType lineType)
    {
        this.lineType = lineType;

        if(lineType == LineType.FIRE) 
        {
            lineRenderer.material = fireMaterial;
        }
        else
        {
            lineRenderer.material = iceMaterial;
        }
    }

    private float GetSizeChange()
    {
        var size = 0f;
        switch (lineType)
        {
            case LineType.FIRE: size = SIZE_CHANGE; break;
            case LineType.ICE: size = -SIZE_CHANGE; break;
        }
        return size;
    }
}
